node {
    stage 'Clean'
        build job: 's2pimb_ap-596_stop', propagate: false
        build job: 's2pimb_ap-596_uninstantiate', propagate: false
        
   stage 'Prepare & deploy environment'
        build job: 's2pimb_ap-596_instantiate'
        build job: 's2pimb_ap-596_start'
        build job: 's2pimb_ap-596_deploy'
        build job: 's2pimb_ap-596_extra_remote-jobs'
        build job: 's2pimb_ap-596_Set_Service_Status_All'
		
    stage 'Restart enviroment after SLR update'
        build job: 's2pimb_ap-596_stop'
		build job: 's2pimb_ap-596_start'
        build job: 's2pimb_ap-596_status'
}