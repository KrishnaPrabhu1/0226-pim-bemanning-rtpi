node {
    stage 'Clean'
        build job: 's2pimb_ap-597_stop', propagate: false
        build job: 's2pimb_ap-597_uninstantiate', propagate: false
        
   stage 'Prepare & deploy environment'
        build job: 's2pimb_ap-597_instantiate'
        build job: 's2pimb_ap-597_start'
        build job: 's2pimb_ap-597_deploy'
        build job: 's2pimb_ap-597_extra_remote-jobs'
        build job: 's2pimb_ap-597_Set_Service_Status_All'
		
    stage 'Restart enviroment after SLR update'
        build job: 's2pimb_ap-597_stop'
		build job: 's2pimb_ap-597_start'
        build job: 's2pimb_ap-597_status'
}