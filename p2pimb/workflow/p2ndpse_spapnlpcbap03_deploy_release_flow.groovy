node {
    stage 'Before deploy release'
        echo "Reinstantiate environment of p2ndpse-node1_spapnlpcbap03"
        build job: "p2ndpse-node1_spapnlpcbap03_stop", propagate: false
        build job: "p2ndpse-node1_spapnlpcbap03_uninstantiate", propagate: false
        build job: "p2ndpse-node1_spapnlpcbap03_instantiate"
        build job: "p2ndpse-node1_spapnlpcbap03_start"
        
        echo "Deploying ${IAR_VERSION} into p2ndpse-node1_spapnlpcbap03"
        build job: "p2ndpse-node1_spapnlpcbap03_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on p2ndpse-node1_spapnlpcbap03'
        build job: "p2ndpse-node1_spapnlpcbap03_stop"
		build job: "p2ndpse-node1_spapnlpcbap03_extra_remote-jobs"
		build job: "p2ndpse-node1_spapnlpcbap03_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "p2ndpse-node1_spapnlpcbap03_start"
        build job: "p2ndpse-node1_spapnlpcbap03_status"
}
