node {
    stage 'Before deploy release'
        echo "Reinstantiate environment of p2ndpse-node2_spapnlpcbap04"
        build job: "p2ndpse-node2_spapnlpcbap04_stop", propagate: false
        build job: "p2ndpse-node2_spapnlpcbap04_uninstantiate", propagate: false
        build job: "p2ndpse-node2_spapnlpcbap04_instantiate"
        build job: "p2ndpse-node2_spapnlpcbap04_start"
        
        echo "Deploying ${IAR_VERSION} into p2ndpse-node2_spapnlpcbap04"
        build job: "p2ndpse-node2_spapnlpcbap04_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on p2ndpse-node2_spapnlpcbap04'
        build job: "p2ndpse-node2_spapnlpcbap04_stop"
		build job: "p2ndpse-node2_spapnlpcbap04_extra_remote-jobs"
		build job: "p2ndpse-node2_spapnlpcbap04_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "p2ndpse-node2_spapnlpcbap04_start"
        build job: "p2ndpse-node2_spapnlpcbap04_status"
}
