p2ndpdk-node2_ap-103 {
    stage 'Before deploy release'
        echo "Reinstantiate environment of p2ndpdk-node2_ap-103"
        build job: "p2ndpdk-node2_ap-103_stop", propagate: false
        build job: "p2ndpdk-node2_ap-103_uninstantiate", propagate: false
        build job: "p2ndpdk-node2_ap-103_instantiate"
        build job: "p2ndpdk-node2_ap-103_start"
        
        echo "Deploying ${IAR_VERSION} into p2ndpdk-node2_ap-103"
        build job: "p2ndpdk-node2_ap-103_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on p2ndpdk-node2_ap-103'
        build job: "p2ndpdk-node2_ap-103_stop"
		build job: "p2ndpdk-node2_ap-103_extra_remote-jobs"
		build job: "p2ndpdk-node2_ap-103_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "p2ndpdk-node2_ap-103_start"
        build job: "p2ndpdk-node2_ap-103_status"
}
