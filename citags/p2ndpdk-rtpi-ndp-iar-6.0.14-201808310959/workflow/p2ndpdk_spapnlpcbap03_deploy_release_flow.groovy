node {
    stage 'Before deploy release'
        echo "Reinstantiate environment of p2ndpdk-node1_ap-102"
        build job: "p2ndpdk-node1_ap-102_stop", propagate: false
        build job: "p2ndpdk-node1_ap-102_uninstantiate", propagate: false
        build job: "p2ndpdk-node1_ap-102_instantiate"
        build job: "p2ndpdk-node1_ap-102_start"
        
        echo "Deploying ${IAR_VERSION} into p2ndpdk-node1_ap-102"
        build job: "p2ndpdk-node1_ap-102_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on p2ndpdk-node1_ap-102'
        build job: "p2ndpdk-node1_ap-102_stop"
		build job: "p2ndpdk-node1_ap-102_extra_remote-jobs"
		build job: "p2ndpdk-node1_ap-102_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "p2ndpdk-node1_ap-102_start"
        build job: "p2ndpdk-node1_ap-102_status"
}
