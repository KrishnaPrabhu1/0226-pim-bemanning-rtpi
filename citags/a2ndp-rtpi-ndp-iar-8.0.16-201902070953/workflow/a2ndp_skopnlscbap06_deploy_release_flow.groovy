node {
    stage 'Before deploy release'
        echo "Reinstantiate environment of a2ndp_skopnlscbap06"
        build job: "a2ndp_skopnlscbap06_stop", propagate: false
        build job: "a2ndp_skopnlscbap06_uninstantiate", propagate: false
        build job: "a2ndp_skopnlscbap06_instantiate"
        build job: "a2ndp_skopnlscbap06_start"
        
        echo "Deploying ${IAR_VERSION} into a2ndp_skopnlscbap06"
        build job: "a2ndp_skopnlscbap06_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on a2ndp_skopnlscbap06'
        build job: "a2ndp_skopnlscbap06_stop"
		build job: "a2ndp_skopnlscbap06_extra_remote-jobs"
		build job: "a2ndp_skopnlscbap06_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "a2ndp_skopnlscbap06_start"
        build job: "a2ndp_skopnlscbap06_status"
}
