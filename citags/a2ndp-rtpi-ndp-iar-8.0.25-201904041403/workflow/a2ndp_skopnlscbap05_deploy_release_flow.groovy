node {
    stage 'Before deploy release'
        echo "Reinstantiate environment of a2ndp_skopnlscbap05"
        build job: "a2ndp_skopnlscbap05_stop", propagate: false
        build job: "a2ndp_skopnlscbap05_uninstantiate", propagate: false
        build job: "a2ndp_skopnlscbap05_instantiate"
        build job: "a2ndp_skopnlscbap05_start"
        
        echo "Deploying ${IAR_VERSION} into a2ndp_skopnlscbap05"
        build job: "a2ndp_skopnlscbap05_deploy_release", parameters: [[$class: 'StringParameterValue', name: 'IAR_VERSION', value: "${IAR_VERSION}"],[$class: 'StringParameterValue', name: 'DISABLE_ELEMENT_CONTROL', value: "${DISABLE_ELEMENT_CONTROL}"]]
    
	stage 'After deploy release on a2ndp_skopnlscbap05'
        build job: "a2ndp_skopnlscbap05_stop"
		build job: "a2ndp_skopnlscbap05_extra_remote-jobs"
		build job: "a2ndp_skopnlscbap05_Set_Service_Status_All", parameters: [[$class: 'StringParameterValue', name: 'STATUS', value: 'OPERATIONAL']]
        build job: "a2ndp_skopnlscbap05_start"
        build job: "a2ndp_skopnlscbap05_status"
}
