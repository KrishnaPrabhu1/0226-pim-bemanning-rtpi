#!/bin/bash

#Activate isp-logrotate
cp $HOME/remote-extra/logrotate/*.conf $RTPI_HOME/jboss.e1/etc/logrotate/
isp-control-rtpi -crontab logrotate on

#Activate slr-update
isp-control-rtpi -crontab servicelookup -rtpe jboss.e1 on

#Remove archived logfiles older then 30 days
timetorun="$(date +"%M %H") * * *"
croncmd="(find $HOME/log/logarchive/* -type f -name '*.gz*' -mtime +31 -exec sh -c 'echo {} >> $HOME/log/logarchive/removed_files.log; rm {}' \;)"
cronjob="$timetorun $croncmd"
crontab -l | fgrep -i -v "$croncmd" | { cat; echo "$cronjob"; } | crontab -

